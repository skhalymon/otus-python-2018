#!/usr/bin/env python3
""" This program used to analyze Nginx logs with following format

log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
                    '$request_time';

"""
__version__ = '0.1.0'

import re
import gzip
import json
import string
import pathlib
import logging
import argparse
import statistics
import collections
import typing as tp
import datetime as dt

############################
# CONFIG
############################

CONFIG = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./logs",
    "SUCCESS_THRESHOLD": 0.8,
    "RENDER_TEMPLATE_PATH": "./reports/report.html"
}


def parse_config(path) -> tp.Dict[str, str]:
    with open(path, 'rb') as config_file:
        conf = json.loads(config_file.read(), encoding='utf-8')
    return conf


def setup_logger(log_path: str = None, log_level=logging.DEBUG):
    if log_path is not None:
        log_path = pathlib.Path(log_path)
        if not log_path.is_dir():
            log_path.mkdir()
        log_path = log_path / 'log_analyzer.log'

    logging.basicConfig(filename=log_path, level=log_level,
                        format='[%(asctime)s] %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')


############################
# LOGS MANIPULATION
############################

def extract_date_from_filename(filename: str,
                               log_match_pattern=re.compile(r"[\w-]+.log-(\d{8})(.gz)?(\w+)?")):
    match = log_match_pattern.match(filename)
    if not match:
        logging.warning(f"Unable to parse date from name: `{filename}`")
        raise ValueError

    raw_date = match.group(1)
    return dt.datetime.strptime(raw_date, "%Y%m%d")  # parse format: 20170630


def get_last_log_file_path(logs_dir_path: pathlib.Path
                           ) -> tp.Optional[pathlib.Path]:
    if not logs_dir_path.exists():
        raise ValueError(f"Logs dir path `{logs_dir_path}` not found.")
    if not logs_dir_path.is_dir():
        raise ValueError(f"You must pass dir path but not file.")

    last_filename = last_date = None
    for p in logs_dir_path.iterdir():
        if p.is_dir():
            logging.debug(f"The {p.name} is dir. Skipped.")
            continue

        filename = p.name

        try:
            file_date = extract_date_from_filename(filename)
        except ValueError:
            continue

        if last_filename is None:
            last_filename = filename
            last_date = file_date
            continue

        if last_date < file_date:
            last_filename = filename
            last_date = file_date

    if last_filename and last_date:
        return logs_dir_path / last_filename


def process_log_line(line: str) -> tp.Tuple[tp.Optional[str], tp.Optional[float]]:
    line = line.split(' ')
    try:
        url, request_time = line[7].strip(), float(line[-1].strip())
        return url, request_time
    except Exception:
        logging.debug(f"Cannot process line: \n\t {line}")
        return None, None


def get_log_records(log_path: tp.Union[str, pathlib.Path],
                    success_threshold: tp.Optional[float] = None) -> tp.Generator:
    log_path = pathlib.Path(log_path)
    open_ = gzip.open if log_path.name.endswith(".gz") else open

    total_logs = succeed_logs = 0
    with open_(log_path, 'rb') as log:
        for line in log:
            total_logs += 1
            url, request_time = process_log_line(line.decode('utf-8'))
            if not url:
                continue

            succeed_logs += 1
            yield url, request_time

    logging.debug(f'Log data processed: '
                  f'\n\tTotal lines: {total_logs}'
                  f'\n\tSucceed logs: {succeed_logs}')

    if not total_logs:
        raise ValueError("The log is empty.")

    if success_threshold is not None and float(succeed_logs) / total_logs < success_threshold:
        raise RuntimeError(f"To much uncorrected logs. "
                           f"\n\tTotal - {total_logs}"
                           f"\n\tSucceed - {succeed_logs}")


############################
# REPORT CREATION
############################

def create_report_item(url, times, succeed_logs, total_rtime):
    return {
        'url': url,
        'count': len(times),
        'count_perc': round(float(len(times)) / succeed_logs, 3),
        'time_avg': round(sum(times) / len(times), 3),
        'time_max': max(times),
        'time_med': statistics.median(times),
        'time_perc': round(sum(times) / total_rtime, 3),
        'time_sum': sum(times)
    }


def prepare_data(logs: tp.Generator) -> tp.List[tp.Dict]:
    total_logs = succeed_logs = total_rtime = 0
    parsed_urls = collections.defaultdict(list)
    for url, request_time in logs:
        total_logs += 1
        if not url:
            continue

        parsed_urls[url].append(request_time)
        succeed_logs += 1
        total_rtime += request_time

    return sorted([create_report_item(url, times, succeed_logs, total_rtime)
                   for url, times in parsed_urls.items()],
                  key=lambda x: x['time_avg'], reverse=True)


def render_report(data, report_file, template_path):
    with open(template_path, 'rb') as template_file:
        template_string = template_file.read().decode('utf-8')
        template = string.Template(template_string)

    report = template.safe_substitute(table_json=json.dumps(data))
    with open(report_file, 'wb') as render_report_file:
        render_report_file.write(report.encode('utf-8'))


def main(config: tp.Dict):
    log_path = get_last_log_file_path(pathlib.Path(config['LOG_DIR']))

    if not log_path:
        logging.info("There no supported log files.")

    log_date = extract_date_from_filename(log_path.name).strftime('%Y.%m.%d')
    report_file = pathlib.Path(config['REPORT_DIR']) / f"report-{log_date}.html"
    if report_file.exists():
        logging.warning(f"The report `{report_file.name}` already exists")
        return None

    log_records = get_log_records(log_path, float(config['SUCCESS_THRESHOLD']))
    data = prepare_data(log_records)
    logging.debug(f"Log parsed: {len(data)} lines.")
    report_size = int(config['REPORT_SIZE'])
    render_report(data[:report_size], report_file, config['RENDER_TEMPLATE_PATH'])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Parse config')
    parser.add_argument('-c', '--config', type=str, nargs='?', dest='config')
    setup_logger(log_level=logging.DEBUG)

    copied_config = CONFIG.copy()
    args = parser.parse_args()

    if args.config:
        config_path = pathlib.Path(args.config)
        logging.debug(f"Config path: {config_path}")
        if not config_path.exists():
            raise ValueError(f"Config path doesn't exist: \n\t{config_path}")
        parsed_config = parse_config(config_path)
        logging.debug(f"Parsed configs: {parsed_config}")
        copied_config.update(parsed_config)

    logging.debug(f"Config: {copied_config}")
    try:
        main(copied_config)
    except Exception as e:
        logging.exception('Exception has been raised.')
