import base64
import gzip
import pathlib
import tempfile
import unittest
import datetime as dt

import log_analyzer as lg

LOGS = b"""
1.196.116.32 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/25019354 HTTP/1.1" 200 927 "-" "Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498697422-2190034393-4708-9752759" "dc7161be3" 0.390
1.99.174.176 3b81f63526fa8  - [29/Jun/2017:03:50:22 +0300] "GET /api/1/photogenic_banners/list/?server_name=WIN7RB4 HTTP/1.1" 200 12 "-" "Python-urllib/2.7" "-" "1498697422-32900793-4708-9752770" "-" 0.133
1.169.137.128 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/16852664 HTTP/1.1" 200 19415 "-" "Slotovod" "-" "1498697422-2118016444-4708-9752769" "712e90144abee9" 0.199
1.199.4.96 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/slot/4705/groups HTTP/1.1" 200 2613 "-" "Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498697422-3800516057-4708-9752745" "2a828197ae235b0b3cb" 0.704
1.168.65.96 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/internal/banner/24294027/info HTTP/1.1" 200 407 "-" "-" "-" "1498697422-2539198130-4709-9928846" "89f7f1be37d" 0.146
1.169.137.128 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/group/1769230/banners HTTP/1.1" 200 1020 "-" "Configovod" "-" "1498697422-2118016444-4708-9752747" "712e90144abee9" 0.628
1.194.135.240 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/group/7786679/statistic/sites/?date_type=day&date_from=2017-06-28&date_to=2017-06-28 HTTP/1.1" 200 22 "-" "python-requests/2.13.0" "-" "1498697422-3979856266-4708-9752772" "8a7741a54297568b" 0.067
1.169.137.128 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/1717161 HTTP/1.1" 200 2116 "-" "Slotovod" "-" "1498697422-2118016444-4708-9752771" "712e90144abee9" 0.138
"""


class TestReading(unittest.TestCase):
    def setUp(self):
        self.render_template_path = pathlib.Path('./reports/report.html')
        self.logs_dir = tempfile.TemporaryDirectory()
        self.logs_dir_path = pathlib.Path(self.logs_dir.name)

        self.reports_dir = tempfile.TemporaryDirectory()
        self.reports_dir_path = pathlib.Path(self.reports_dir.name)

        self.raw_log_file = tempfile.NamedTemporaryFile(dir=self.logs_dir_path,
                                                        prefix='nginx-access-ui.log-20170630', delete=False)
        self.raw_log_file.write(LOGS)
        self.raw_log_file.seek(0)

        self.gz_log_file = tempfile.NamedTemporaryFile(dir=self.logs_dir_path,
                                                       prefix='nginx-access-ui.log-20170630',
                                                       suffix='.gz', delete=False)
        with gzip.open(self.gz_log_file.name, 'wb') as f:
            f.write(LOGS)

    def tearDown(self):
        self.logs_dir.cleanup()
        self.reports_dir.cleanup()

    def test_get_log_records(self):
        self.assertTrue(next(lg.get_log_records(self.raw_log_file.name)))
        self.assertTrue(next(lg.get_log_records(self.gz_log_file.name)))

    def test_extract_date_from_filename(self):
        self.assertEqual(lg.extract_date_from_filename('nginx-access-ui.log-20170630'),
                         dt.datetime(year=2017, month=6, day=30))
        self.assertEqual(lg.extract_date_from_filename('nginx-access-ui.log-20170630.gz'),
                         dt.datetime(year=2017, month=6, day=30))
        with self.assertRaises(ValueError):
            lg.extract_date_from_filename('nginx-access20170630')

    def test_parse_last_log_from_logs_dir(self):
        expected = self.logs_dir_path / 'nginx-access-ui.log-20170630'
        expected_date = lg.extract_date_from_filename(expected.name)
        last_log_path = lg.get_last_log_file_path(self.logs_dir_path)
        last_log_date = lg.extract_date_from_filename(last_log_path.name)
        self.assertEqual(last_log_date, expected_date)

    def test_prepare_data(self):
        logs = lg.get_log_records(self.raw_log_file.name)
        data = lg.prepare_data(logs)
        self.assertSequenceEqual(data[0],
                                 {'url': '/api/v2/slot/4705/groups', 'count': 1, 'count_perc': 0.125, 'time_avg': 0.704,
                                  'time_max': 0.704, 'time_med': 0.704, 'time_perc': 0.293, 'time_sum': 0.704}
                                 )

    def test_render_report(self):
        logs = lg.get_log_records(self.gz_log_file.name)
        log_date = '20170630'
        report_file = self.reports_dir_path / f"report-{log_date}.html"
        lg.render_report(lg.prepare_data(logs),
                         report_file,
                         self.render_template_path
                         )
        self.assertTrue(report_file.exists())


if __name__ == '__main__':
    unittest.main()
